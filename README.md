# Gryph NLU Intents CI/CD

This repo stores the config files for the Integration and Production environments. 
Each environment has a folder structure as such:

`
 |
 |
 +_____ <EnvironmentName>
    |
    +____ ConversationProfile/
    |       |
    |       +___ ConversationProfile.json
    |
    |   
    +____ Agents/
            |
            +___ Agent_Customer/
            |
            +___ Agent_Human/
     



When a commit is made to this repo, the changes will be consumed and deployed by a
Jenkins job at https://jenkins.gryphonnetworks.com/job/conversation-ai-ci-cd/

This will result in a log file that will provide the following information:

- The commit that triggered the build
- Whether this commit has created sufficient changes (the new files are different and syntactically legal)
- Announcement of changes being submitted
- Response from Dialogflow or AgentAssist server 

At this point, the build will either succeed (200 resp) or fail. 
If the build fails, the error will be printed with the details of the commit that failed.
